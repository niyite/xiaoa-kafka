package com.xiaoa.kafka.test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.annotation.Resource;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.DescribeTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaOperations;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.xiaoa.kafka.handler.KafkaSendResultHandler;

/**
 * @author xiaoa
 * @apiNote kafka默认异步发消息，要使用Thread.sleep()解决
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class KafkaTest {
	@Autowired
	private KafkaTemplate<Integer, String> kafkaTemplate;

	@Test
	public void testDemo() throws InterruptedException {
		kafkaTemplate.send("topic.quick.demo", "this is my first demo");
		// 休眠5秒，为了使监听器有足够的时间监听到topic的数据
		Thread.sleep(5000);
	}

	@Autowired
	private AdminClient adminClient;

	@Test
	public void testCreateTopic() throws InterruptedException {
		NewTopic topic = new NewTopic("topic.quick.initial2", 1, (short) 1);
		adminClient.createTopics(Arrays.asList(topic));
		Thread.sleep(1000);
	}

	@Test
	public void testSelectTopicInfo() throws ExecutionException, InterruptedException {
		DescribeTopicsResult result = adminClient.describeTopics(Arrays.asList("topic.quick.initial"));
		result.all().get().forEach((k, v) -> System.out.println("k: " + k + " ,v: " + v.toString() + "\n"));
	}

	@Resource
	private KafkaTemplate<Integer, String> defaultKafkaTemplate;

	@Test
	public void testDefaultKafkaTemplate() {
		defaultKafkaTemplate.sendDefault("I`m send msg to default topic");
	}

	@Test
	public void testTemplateSend() {
		// 发送带有时间戳的消息
		kafkaTemplate.send("topic.quick.demo", 0, System.currentTimeMillis(), 0, "send message with timestamp");

		// 使用ProducerRecord发送消息
		ProducerRecord<Integer, String> record = new ProducerRecord<Integer, String>("topic.quick.demo", "use ProducerRecord to send message");
		kafkaTemplate.send(record);

		// 使用Message发送消息
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(KafkaHeaders.TOPIC, "topic.quick.demo");
		map.put(KafkaHeaders.PARTITION_ID, 0);
		map.put(KafkaHeaders.MESSAGE_KEY, 0);
		GenericMessage<String> message = new GenericMessage<String>("use Message to send message", new MessageHeaders(map));
		kafkaTemplate.send(message);
	}

	@Autowired
	private KafkaSendResultHandler producerListener;

	/**
	 * 带监听器，监听失败或成功
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testProducerListen() throws InterruptedException {
		kafkaTemplate.setProducerListener(producerListener);
		kafkaTemplate.send("topic.quick.demo", "test producer listen");
		Thread.sleep(1000);
	}

	/**
	 * 同步发送消息，加上get()就可以了
	 * 
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	@Test
	public void testSyncSend() throws ExecutionException, InterruptedException {
		kafkaTemplate.send("topic.quick.demo", "test sync send message").get();
	}

	/**
	 * 同步发送，1ms内不成功就报异常
	 * 
	 * @throws ExecutionException
	 * @throws InterruptedException
	 * @throws TimeoutException
	 */
	@Test
	public void testTimeOut() throws ExecutionException, InterruptedException, TimeoutException {
		kafkaTemplate.send("topic.quick.demo", "test send message timeout").get(1, TimeUnit.MICROSECONDS);
	}

	@Resource
	private KafkaTemplate<Integer, String> kafkaTemplateWithTransaction;

	/**
	 * 需要配置KafkaTransactionManager才可以使用事务
	 * 
	 * @throws InterruptedException
	 */
	@Test
	@Transactional
	public void testTransactionalAnnotation() throws InterruptedException {
		kafkaTemplateWithTransaction.send("topic.quick.tran", "test transactional annotation");
//		throw new RuntimeException("fail");
		Thread.sleep(1000);
	}

	/**
	 * 本地事务，不需要其它配置
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testExecuteInTransaction() throws InterruptedException {
		kafkaTemplateWithTransaction.executeInTransaction(new KafkaOperations.OperationsCallback<Integer, String, Object>() {
			@Override
			public Object doInOperations(KafkaOperations<Integer, String> kafkaOperations) {
				kafkaOperations.send("topic.quick.tran", "test executeInTransaction");
//				throw new RuntimeException("fail");
				return true;
			}
		});
	}

	/**
	 * 测试配置方式实现监听
	 */
	@Test
	public void test() {
		kafkaTemplate.send("topic.quick.bean", "send msg to beanListener");
	}
}
