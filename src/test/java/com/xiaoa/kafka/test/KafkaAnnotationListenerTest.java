package com.xiaoa.kafka.test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.kafka.requestreply.RequestReplyFuture;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xiaoa
 * @apiNote 测试kafka注解监听
 *
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class KafkaAnnotationListenerTest {

	@Autowired
	private KafkaTemplate<Integer, String> kafkaTemplate;

	@Test
	public void testConsumerRecord() throws InterruptedException {
		kafkaTemplate.send("topic.quick.consumer", "test receive by consumerRecord");
		Thread.sleep(5000);
	}

	@Test
	public void testBatch() throws InterruptedException {
		for (int i = 0; i < 12; i++) {
			kafkaTemplate.send("topic.quick.batch", "test batch listener,dataNum-" + i);
		}
		Thread.sleep(5000);
	}

	@Test
	public void testBatchPartition() throws InterruptedException {
		for (int i = 0; i < 12; i++) {
			kafkaTemplate.send("topic.quick.batch.partition", "test batch listener,dataNum-" + i);
		}
		Thread.sleep(5000);
	}

	@Test
	public void testAnno() throws InterruptedException {
		Map<String, Object> map = new HashMap<>();
		map.put(KafkaHeaders.TOPIC, "topic.quick.anno");
		map.put(KafkaHeaders.MESSAGE_KEY, 0);
		map.put(KafkaHeaders.PARTITION_ID, 0);
		map.put(KafkaHeaders.TIMESTAMP, System.currentTimeMillis());

		kafkaTemplate.send(new GenericMessage<String>("test anno listener", map));
		Thread.sleep(5000);
	}

	@Test
	public void testAck() throws InterruptedException {
		for (int i = 0; i < 5; i++) {
			kafkaTemplate.send("topic.quick.ack", i + "");
		}
		Thread.sleep(5000);
	}

	@Test
	public void testForward() throws InterruptedException {
		kafkaTemplate.send("topic.quick.target", "test @SendTo");
		Thread.sleep(5000);
	}

	/**
	 * 发送消息就显得稍微有点麻烦了，不过在项目编码过程中可以把它封装成一个工具类调用。
	 * 1.我们需要创建ProducerRecord类，用来发送消息，并添加KafkaHeaders.REPLY_TOPIC到record的headers参数中，这个参数配置我们想要转发到哪个Topic中。
	 * 2.使用replyingKafkaTemplate.sendAndReceive()方法发送消息，该方法返回一个Future类RequestReplyFuture，这里类里面包含了获取发送结果的Future类和获取返回结果的Future类。使用replyingKafkaTemplate发送及返回都是异步操作。
	 * 3.调用RequestReplyFuture.getSendFutrue().get()方法可以获取到发送结果
	 * 4.调用RequestReplyFuture.get()方法可以获取到响应结果
	 * 注意：由于ReplyingKafkaTemplate也是通过监听容器实现的，所以响应时间可能会较慢，要注意选择合适的场景使用。
	 * 
	 */
	@Autowired
	private ReplyingKafkaTemplate<Integer, String, String> replyingKafkaTemplate;

	@Test
	public void testReplyingKafkaTemplate() throws ExecutionException, InterruptedException, TimeoutException {
		ProducerRecord<Integer, String> record = new ProducerRecord<>("topic.quick.request", "this is a message");
		record.headers().add(new RecordHeader(KafkaHeaders.REPLY_TOPIC, "topic.quick.reply".getBytes()));
		RequestReplyFuture<Integer, String, String> replyFuture = replyingKafkaTemplate.sendAndReceive(record);
		SendResult<Integer, String> sendResult = replyFuture.getSendFuture().get();
		System.out.println("Sent ok: " + sendResult.getRecordMetadata());
		ConsumerRecord<Integer, String> consumerRecord = replyFuture.get();
		System.out.println("Return value: " + consumerRecord.value());
		Thread.sleep(20000);
	}

	@Test
	public void testTask() throws InterruptedException {
		for (int i = 0; i < 10; i++) {
			kafkaTemplate.send("topic.quick.durable", "this is durable message");
		}
		Thread.sleep(5000);
	}

	@Test
	public void testFilter() throws InterruptedException {
		for (int i = 0; i < 10; i++) {
			kafkaTemplate.send("topic.quick.filter", i + "");
		}
		Thread.sleep(5000);
	}

	@Test
	public void testErrorHandler() throws InterruptedException {
		kafkaTemplate.send("topic.quick.error", "test error handle");
		Thread.sleep(5000);
	}

}
