package com.xiaoa.kafka.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.listener.config.ContainerProperties;
import org.springframework.kafka.transaction.KafkaTransactionManager;

@Configuration
@EnableKafka
public class KafkaConfiguration {

	// 生产者配置
	private Map<String, Object> senderProps() {
		Map<String, Object> props = new HashMap<>();
		// 连接地址
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.144.129:9092");
		// 重试，0为不启用重试机制
		props.put(ProducerConfig.RETRIES_CONFIG, 1);
		// 控制批处理大小，单位为字节
		props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);
		// 批量发送，延迟为1毫秒，启用该功能能有效减少生产者发送消息次数，从而提高并发量
		props.put(ProducerConfig.LINGER_MS_CONFIG, 1);
		// 生产者可以使用的总内存字节来缓冲等待发送到服务器的记录
		props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 1024000);
		// 键的序列化方式
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class);
		// 值的序列化方式
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		return props;
	}

	// 消费者配置参数
	private Map<String, Object> consumerProps() {
		Map<String, Object> props = new HashMap<>();
		// 连接地址
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.144.129:9092");
		// GroupID
		props.put(ConsumerConfig.GROUP_ID_CONFIG, "bootKafka");
		// 是否自动提交
		props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
		// 自动提交的频率
		props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "100");
		// Session超时设置
		props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "15000");
		// 键的反序列化方式
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, IntegerDeserializer.class);
		// 值的反序列化方式
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		return props;
	}

	// ConcurrentKafkaListenerContainerFactory为创建Kafka监听器的工程类，这里只配置了消费者
	@Bean
	public ConcurrentKafkaListenerContainerFactory<Integer, String> kafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<Integer, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(consumerFactory());
		return factory;
	}

	// 用于转发的容器工厂类
	@Bean("forwardContainerFactory")
	public ConcurrentKafkaListenerContainerFactory<Integer, String> forwardContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<Integer, String> factory = new ConcurrentKafkaListenerContainerFactory<Integer, String>();
		factory.setConsumerFactory(consumerFactory());
		factory.setReplyTemplate(kafkaTemplate());
		return factory;
	}

	// 根据senderProps填写的参数创建生产者工厂
	@Bean
	public ProducerFactory<Integer, String> producerFactory() {
		return new DefaultKafkaProducerFactory<>(senderProps());
	}

	// 带事务的生产者工厂
	@Bean
	public ProducerFactory<Integer, String> producerFactoryWithTransaction() {
		DefaultKafkaProducerFactory<Integer, String> factory = new DefaultKafkaProducerFactory<>(senderProps());
		factory.transactionCapable();
		factory.setTransactionIdPrefix("tran-");
		return factory;
	}

	// 根据consumerProps填写的参数创建消费者工厂
	@Bean
	public ConsumerFactory<Integer, String> consumerFactory() {
		return new DefaultKafkaConsumerFactory<>(consumerProps());
	}

	// 不带事务的KafkaTemplate，加入@Primary注解后默认注入
	@Bean
	@Primary
	public KafkaTemplate<Integer, String> kafkaTemplate() {
		KafkaTemplate<Integer, String> template = new KafkaTemplate<Integer, String>(producerFactory());
		return template;
	}

	// 带事务的KafkaTemplate
	@Bean("kafkaTemplateWithTransaction")
	public KafkaTemplate<Integer, String> kafkaTemplateWithTransaction() {
		KafkaTemplate<Integer, String> template = new KafkaTemplate<Integer, String>(producerFactoryWithTransaction());
		return template;
	}

	// 指定topic的KafkaTemplate
	@Bean("defaultKafkaTemplate")
	public KafkaTemplate<Integer, String> defaultKafkaTemplate() {
		KafkaTemplate<Integer, String> template = new KafkaTemplate<Integer, String>(producerFactory());
		template.setDefaultTopic("topic.quick.default");
		return template;
	}

	// 需要配置事务管理器才可以使用@Transactional
	@Bean
	public KafkaTransactionManager<Integer, String> transactionManager(ProducerFactory<Integer, String> producerFactoryWithTransaction) {
		KafkaTransactionManager<Integer, String> manager = new KafkaTransactionManager<Integer, String>(producerFactoryWithTransaction);
		return manager;
	}

	// 配置方式实现监听
	@Bean
	public KafkaMessageListenerContainer<Integer, String> demoListenerContainer() {
		ContainerProperties properties = new ContainerProperties("topic.quick.bean");
		properties.setGroupId("bean");
		properties.setMessageListener(new MessageListener<Integer, String>() {
			private Logger log = LoggerFactory.getLogger(this.getClass());

			@Override
			public void onMessage(ConsumerRecord<Integer, String> record) {
				log.info("topic.quick.bean receive : " + record.toString());
			}
		});
		return new KafkaMessageListenerContainer<Integer, String>(consumerFactory(), properties);
	}

}
