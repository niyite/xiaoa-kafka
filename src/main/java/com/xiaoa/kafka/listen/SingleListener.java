package com.xiaoa.kafka.listen;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class SingleListener {
	private static final Logger LOGGER = LoggerFactory.getLogger(SingleListener.class);

	@KafkaListener(id = "consumer", topics = "topic.quick.consumer")
	public void consumerListener(ConsumerRecord<Integer, String> record) {
		LOGGER.info("topic.quick.consumer receive : " + record.toString());
	}
	
}
