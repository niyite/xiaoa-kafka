package com.xiaoa.kafka.listen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.config.ContainerProperties;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

/**
 * Spring-Kafka整合了两种消息转发方式：
 * 使用Headers设置回复主题（Reply_Topic），这种方式比较特别，是一种请求响应模式，使用的是ReplyingKafkaTemplate类
 * 手动转发，使用@SendTo注解将监听方法返回值转发到Topic中
 * 
 * @author xiaoa 自动转发:
 * @apiNote 1.@SendTo方式
 *          1.1.配置ConcurrentKafkaListenerContainerFactory的ReplyTemplate
 *          1.2.监听方法加上@SendTo注解
 * 
 *          2.使用Headers设置回复主题（Reply_Topic）
 *          2.1.配置ConcurrentKafkaListenerContainerFactory的ReplyTemplate
 *          2.2.配置topic.quick.request的监听器
 *          2.3.注册一个KafkaMessageListenerContainer类型的监听容器，监听topic.quick.reply，这个监听器里面我们不处理任何事情，交由ReplyingKafkaTemplate处理
 *          2.4.通过ProducerFactory和KafkaMessageListenerContainer创建一个ReplyingKafkaTemplate类型的Bean，设置回复超时时间为10
 * 
 */
@Component
public class ForwardListener {
	private static final Logger LOGGER = LoggerFactory.getLogger(ForwardListener.class);

	// 手动转发
	@KafkaListener(id = "forward", topics = "topic.quick.target", containerFactory = "forwardContainerFactory")
	@SendTo("topic.quick.real")
	public String forward(String data) {
		LOGGER.info("topic.quick.target  forward " + data + " to  topic.quick.real");
		return "topic.quick.target send msg : " + data;
	}

	// 利用Headers转发
	@KafkaListener(id = "replyConsumer", topics = "topic.quick.request", containerFactory = "forwardContainerFactory")
	@SendTo
	public String replyListen(String msgData) {
		LOGGER.info("topic.quick.request receive : " + msgData);
		return "topic.quick.reply  reply : " + msgData;
	}

	@Bean
	public KafkaMessageListenerContainer<Integer, String> replyContainer(@Autowired ConsumerFactory<Integer, String> consumerFactory) {
		ContainerProperties containerProperties = new ContainerProperties("topic.quick.reply");
		return new KafkaMessageListenerContainer<Integer, String>(consumerFactory, containerProperties);
	}

	@Bean
	public ReplyingKafkaTemplate<Integer, String, String> replyingKafkaTemplate(@Autowired ProducerFactory<Integer, String> producerFactory,
			KafkaMessageListenerContainer<Integer, String> replyContainer) {
		ReplyingKafkaTemplate<Integer, String, String> template = new ReplyingKafkaTemplate<Integer, String, String>(producerFactory, replyContainer);
		template.setReplyTimeout(10000);
		return template;
	}

}
