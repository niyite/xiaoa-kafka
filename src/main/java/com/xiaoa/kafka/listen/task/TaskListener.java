package com.xiaoa.kafka.listen.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author xiaoa
 * @apiNote 定时启动监听
 */
@Component
@EnableScheduling
public class TaskListener {
	private static final Logger LOGGER = LoggerFactory.getLogger(TaskListener.class);

	@Autowired
	private KafkaListenerEndpointRegistry registry;

	@Autowired
	private ConsumerFactory<Integer, String> consumerFactory;

	@Bean
	public ConcurrentKafkaListenerContainerFactory<Integer, String> delayContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<Integer, String> container = new ConcurrentKafkaListenerContainerFactory<Integer, String>();
		container.setConsumerFactory(consumerFactory);
		// 禁止自动启动
		container.setAutoStartup(false);
		return container;
	}

	@KafkaListener(id = "durable", topics = "topic.quick.durable", containerFactory = "delayContainerFactory")
	public void durableListener(String data) {
		// 这里做数据持久化的操作
		LOGGER.info("topic.quick.durable receive : " + data);
	}

	// 定时器，每天凌晨0点开启监听
	@Scheduled(cron = "0 0 0 * * ?")
	public void startListener() {
		LOGGER.info("开启监听");
		// 判断监听容器是否启动，未启动则将其启动
		if (!registry.getListenerContainer("durable").isRunning()) {
			registry.getListenerContainer("durable").start();
		}
		registry.getListenerContainer("durable").resume();
	}

	// 定时器，每天早上10点关闭监听
	@Scheduled(cron = "0 0 10 * * ?")
	public void shutDownListener() {
		LOGGER.info("关闭监听");
		registry.getListenerContainer("durable").pause();
	}

}