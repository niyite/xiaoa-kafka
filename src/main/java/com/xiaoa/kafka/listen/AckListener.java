package com.xiaoa.kafka.listen;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.AbstractMessageListenerContainer;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

/**
 * @author xiaoa 使用Kafka的Ack机制比较简单，只需简单的三步即可：
 * @apiNote 1.设置ENABLE_AUTO_COMMIT_CONFIG=false，禁止自动提交
 * @apiNote 2.设置AckMode=MANUAL_IMMEDIATE
 * @apiNote 3.监听方法加入Acknowledgment ack 参数
 * @apiNote 怎么拒绝消息呢，只要在监听方法中不调用ack.acknowledge()即可
 * 
 */
@Component
public class AckListener {
	private static final Logger LOGGER = LoggerFactory.getLogger(AckListener.class);

	private Map<String, Object> consumerProps() {
		Map<String, Object> props = new HashMap<>();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.144.129:9092");
		props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
		props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
		props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "15000");
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, IntegerDeserializer.class);
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		return props;
	}

	@Bean("ackContainerFactory")
	public ConcurrentKafkaListenerContainerFactory<Integer, String> ackContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<Integer, String> factory = new ConcurrentKafkaListenerContainerFactory<Integer, String>();
		factory.setConsumerFactory(new DefaultKafkaConsumerFactory<Integer, String>(consumerProps()));
		factory.getContainerProperties().setAckMode(AbstractMessageListenerContainer.AckMode.MANUAL_IMMEDIATE);
		factory.setConsumerFactory(new DefaultKafkaConsumerFactory<Integer, String>(consumerProps()));
		return factory;
	}

	@KafkaListener(id = "ack", topics = "topic.quick.ack", containerFactory = "ackContainerFactory")
	public void ackListener(ConsumerRecord<Integer, String> record, Acknowledgment ack) {
		LOGGER.info("topic.quick.ack exist receive : " + record.toString());
		ack.acknowledge();
	}

	
	/**
	 * @apiNote 下面两种方法解决拒绝ack后，消息无法消费的问题
	 * @apiNote 1.将没有消费的消息重新发回kafka
	 * @apiNote 2.用consumer.seek方法将offset重新定位到没有消费的那条消息，可能造成死循环
	 */
//	@Autowired
//	private KafkaTemplate<Integer, String> kafkaTemplate;

//	@KafkaListener(id = "ack", topics = "topic.quick.ack", containerFactory = "ackContainerFactory")
//	public void ackListener(ConsumerRecord<Integer, String> record, Acknowledgment ack, Consumer<Integer, String> consumer) throws InterruptedException, ExecutionException {
//		LOGGER.info("topic.quick.ack receive : " + record.value());
//		// 如果偏移量为偶数则确认消费，否则拒绝消费
//		if (record.offset() % 2 == 0) {
//			LOGGER.info(record.offset() + "--ack");
//			ack.acknowledge();
//		} else {
//			LOGGER.info(record.offset() + "--nack");
//			kafkaTemplate.send("topic.quick.ack", record.value()).get();
//		}
//	}

//	@KafkaListener(id = "ack", topics = "topic.quick.ack", containerFactory = "ackContainerFactory")
//	public void ackListener(ConsumerRecord<Integer, String> record, Acknowledgment ack, Consumer<Integer, String> consumer) {
//		LOGGER.info("topic.quick.ack receive : " + record.value());
//
//		// 如果偏移量为偶数则确认消费，否则拒绝消费
//		if (record.offset() % 2 == 0) {
//			LOGGER.info(record.offset() + "--ack");
//			ack.acknowledge();
//		} else {
//			LOGGER.info(record.offset() + "--nack");
//			consumer.seek(new TopicPartition("topic.quick.ack", record.partition()), record.offset());
//		}
//	}
	
}
