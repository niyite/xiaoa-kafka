package com.xiaoa.kafka.listen.error;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.ConsumerAwareListenerErrorHandler;
import org.springframework.kafka.listener.ListenerExecutionFailedException;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;

@Component
public class ErrorListener {
	private static final Logger LOGGER = LoggerFactory.getLogger(ErrorListener.class);

	@KafkaListener(id = "err", topics = "topic.quick.error", errorHandler = "consumerAwareErrorHandler")
	public void errorListener(String data) {
		LOGGER.info("topic.quick.error receive : " + data);
		throw new RuntimeException("fail");
	}

	@Bean
	public ConsumerAwareListenerErrorHandler consumerAwareErrorHandler() {
		return new ConsumerAwareListenerErrorHandler() {

			@Override
			public Object handleError(Message<?> message, ListenerExecutionFailedException e, Consumer<?, ?> consumer) {
				LOGGER.info("consumerAwareErrorHandler receive : " + message.getPayload().toString());
				return null;
			}
		};
	}

	@Bean
	public ConsumerAwareListenerErrorHandler batchConsumerAwareErrorHandler() {
		return new ConsumerAwareListenerErrorHandler() {

			@Override
			public Object handleError(Message<?> message, ListenerExecutionFailedException e, Consumer<?, ?> consumer) {
				LOGGER.info("consumerAwareErrorHandler receive : " + message.getPayload().toString());
				MessageHeaders headers = message.getHeaders();
				List<?> topics = headers.get(KafkaHeaders.RECEIVED_TOPIC, List.class);
				List<?> partitions = headers.get(KafkaHeaders.RECEIVED_PARTITION_ID, List.class);
				List<?> offsets = headers.get(KafkaHeaders.OFFSET, List.class);
				Map<TopicPartition, Long> offsetsToReset = new HashMap<>();

				LOGGER.info("topics:{},partitions:{},offsets:{},offsetsToReset:{}", topics, partitions, offsets, offsetsToReset);

				return null;
			}
		};
	}

}
