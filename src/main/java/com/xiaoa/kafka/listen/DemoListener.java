package com.xiaoa.kafka.listen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class DemoListener {
	private static final Logger LOGGER = LoggerFactory.getLogger(DemoListener.class);

	// 声明consumerID为demo，监听topicName为topic.quick.demo的Topic
	@KafkaListener(id = "demo", topics = "topic.quick.demo")
	public void listen(String msgData) {
		LOGGER.info("demo receive : " + msgData);
	}
}
