package com.xiaoa.kafka.listen;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.PartitionOffset;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * @author xiaoa
 * @apiNote 同时消费的消息数为MAX_POLL_RECORDS_CONFIG*Concurrency，即单线程拉取消息数与并发数的乘积，并发数不能大于分区数，要提高呑吐量可以增加分区数
 *
 */
@Component
public class BatchListener {
	private static final Logger LOGGER = LoggerFactory.getLogger(BatchListener.class);

	private Map<String, Object> consumerProps() {
		Map<String, Object> props = new HashMap<>();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.144.129:9092");
		props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
		props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
		props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "15000");
		// 一次拉取消息数量，一个线程同时摘取的消息数
		props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "5");
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, IntegerDeserializer.class);
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		return props;
	}

	@Bean("batchContainerFactory")
	public ConcurrentKafkaListenerContainerFactory<Integer, String> listenerContainer() {
		ConcurrentKafkaListenerContainerFactory<Integer, String> factory = new ConcurrentKafkaListenerContainerFactory<Integer, String>();
		factory.setConsumerFactory(new DefaultKafkaConsumerFactory<Integer, String>(consumerProps()));
		// 设置并发量，小于或等于Topic的分区数，相当于创建这么多个线程同时接收消息
		factory.setConcurrency(5);
		// 设置为批量监听
		factory.setBatchListener(true);
		return factory;
	}

	@Bean
	public NewTopic batchTopic() {
		return new NewTopic("topic.quick.batch", 8, (short) 1);
	}

	@KafkaListener(id = "batch", clientIdPrefix = "batch", topics = { "topic.quick.batch" }, containerFactory = "batchContainerFactory")
	public void batchListener(List<String> datas) {
		LOGGER.info("topic.quick.batch  receive : ");
		for (String data : datas) {
			LOGGER.info(data);
		}
	}

	@Bean
	public NewTopic batchWithPartitionTopic() {
		return new NewTopic("topic.quick.batch.partition", 8, (short) 1);
	}

	@KafkaListener(id = "batchWithPartition", clientIdPrefix = "bwp", containerFactory = "batchContainerFactory", topicPartitions = {
			@TopicPartition(topic = "topic.quick.batch.partition", partitions = { "1", "3" }),
			@TopicPartition(topic = "topic.quick.batch.partition", partitions = { "0", "4" }, partitionOffsets = @PartitionOffset(partition = "2", initialOffset = "100")) })
	public void batchListenerWithPartition(List<String> datas) {
		LOGGER.info("topic.quick.batch.partition  receive : ");
		for (String data : datas) {
			LOGGER.info(data);
		}
	}

	/**
	 * @Payload：获取的是消息的消息体，也就是发送内容
	 * @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY)：获取发送消息的key
	 * @Header(KafkaHeaders.RECEIVED_PARTITION_ID)：获取当前消息是从哪个分区中监听到的
	 * @Header(KafkaHeaders.RECEIVED_TOPIC)：获取监听的TopicName
	 * @Header(KafkaHeaders.RECEIVED_TIMESTAMP)：获取时间戳
	 * @apiNote 如果你想使用批量消费，把对应的类型改为List即可，比如List<String> data ， List<Integer> key。
	 */
	@KafkaListener(id = "anno", topics = "topic.quick.anno")
	public void annoListener(@Payload String data, 
			@Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) Integer key, 
			@Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition,
			@Header(KafkaHeaders.RECEIVED_TOPIC) String topic, 
			@Header(KafkaHeaders.RECEIVED_TIMESTAMP) long ts) {
		LOGGER.info("topic.quick.anno receive : \n" + "data : " + data + "\n" + "key : " + key + "\n" + "partitionId : " + partition + "\n" + "topic : " + topic + "\n" + "timestamp : " + ts + "\n");
	}

}